package main

import (
	"fmt"
	"gitlab.com/yozh/co2-reader-golang/mh_z19"
	"gitlab.com/yozh/google-iot-mqtt-client"
	"log"
	"time"
)

const PROJECT_ID = "massive-physics-671"
const CLOUD_REGION = "europe-west1"
const REGISTRY_ID = "yozh-device-registry"
const DEVICE_ID = "smiss-co2-reader-1"
const DEVICE_PATH = "projects/" + PROJECT_ID + "/locations/" + CLOUD_REGION + "/registries/" + REGISTRY_ID + "/devices/" + DEVICE_ID
const mqttTopic = "/devices/" + DEVICE_ID + "/events"

func main() {
	mqttClient := google_iot_mqtt_client.GoogleMqttClient{}
	e := mqttClient.Connect(PROJECT_ID, DEVICE_PATH, "./resources/smiss-co2-reader/rsa_private.pem", "./resources/google-certs/roots.pem")
	if e != nil {
		log.Fatal("Cannot connect to MQTT", e)
	}

	failedSendAttempts := 0
	for {
		co2, _ := mh_z19.Read()

		//if e == nil {
		message := fmt.Sprintf("{\"value\":%v, \"date\":%v}", co2, time.Now().UnixNano()/1000000)
		log.Println("Sending " + message)
		token := mqttClient.Client.Publish(mqttTopic, 1, false, message)
		timedOut := !token.WaitTimeout(time.Second * 30)

		if token.Error() != nil {
			log.Println("Error publishing result", token.Error())
			failedSendAttempts++
		} else if timedOut {
			log.Println("Send timed out")
			failedSendAttempts++
		}
		//} else {
		//	log.Println("Error while reading co2 value", e)
		//}
		log.Println("Sleeping")
		time.Sleep(time.Duration(time.Second * 60))
	}
}
