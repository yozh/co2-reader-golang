package mh_z19

import (
	"errors"
	//"fmt"
	"github.com/tarm/serial"
	"os/exec"
)

const MaxReadRetries = 10
const MaxConnectRetries = 2

func Read() (uint, error) {
	_ = exec.Command("systemctl", "stop", "serial-getty@ttyAMA0.service").Run()

	for connectRetries := 0; connectRetries < MaxConnectRetries; connectRetries++ {
		c := &serial.Config{Name: "/dev/ttyAMA0", Baud: 9600}
		s, err := serial.OpenPort(c)
		if err != nil {
			//fmt.Println(err)
			return 0, err
		}

		readCommand := []byte("\xff\x01\x86\x00\x00\x00\x00\x00\x79")

		for readRetries := 0; readRetries < MaxReadRetries; readRetries++ {
			//fmt.Println("Writing")
			n, err := s.Write(readCommand)
			if err == nil {
				buf := make([]byte, 9)
				//fmt.Println("Reading")
				n, err = s.Read(buf)
				if err == nil && n >= 4 && buf[0] == 0xff && buf[1] == 0x86 {
					result := uint(buf[2])*256 + uint(buf[3])
					//fmt.Println(result)
					defer s.Close()
					return result, nil
				} else {
					//fmt.Println("Retrying...")
				}
			} else {
				//fmt.Println(err)
				_ = s.Flush()
			}
		}
		_ = s.Close()
	}

	_ = exec.Command("systemctl", "start", "serial-getty@ttyAMA0.service").Run()

	return 0, errors.New("Couldn't read from serial port")
}
